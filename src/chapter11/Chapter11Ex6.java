/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Eliise
 */
public class Chapter11Ex6 {
     public static void main(String[] args)  {
     
          ArrayList<Integer>numbers = new ArrayList();
   
        numbers.add(3);
        numbers.add(7);
        numbers.add(3);
        numbers.add(-1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(7);
        numbers.add(2);
        numbers.add(15);
        numbers.add(15);
   
   System.out.println(numbers);
   
   countUnique(numbers);
   
   System.out.println("This collection contains " + countUnique(numbers) + " unique numbers");
   
   }

   
       public static int countUnique(ArrayList<Integer>numbers)
       {
       
       HashSet<Integer>set = new HashSet(numbers);
       
       int countUnique = set.size();
       
       return countUnique;
       
       } 
}
