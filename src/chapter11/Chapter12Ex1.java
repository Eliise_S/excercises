/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11;

/**
 *
 * @author Eliise
 */
public class Chapter12Ex1 {
        public static void main(String[] args) {
        starString(3);
    }
    
    public static void starString(int n) {
        if(n < 0)
            throw new IllegalArgumentException("negative exponent");        
        if(n == 0)
            System.out.println("*");
        else {
            for(int i = 0; i < Math.pow(2, n-1); i++)
                System.out.print("*");
            starString(n-1);            
        }
    }
}
