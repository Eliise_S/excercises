/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;


/**
 *
 * @author Eliise
 */
public class Chapter11Ex5 {
    public static void main(String[] args)  {
     
     
       LinkedList<Integer>numbers = new LinkedList();

       numbers.add(7);
       numbers.add(4);
       numbers.add(-9);
       numbers.add(4);
       numbers.add(15);
       numbers.add(8);
       numbers.add(27);
       numbers.add(7);
       numbers.add(11);
       numbers.add(-5);
       numbers.add(32);
       numbers.add(-9);
       numbers.add(-9);

       System.out.println("Before method call " + numbers);

       SortAndRemoveDuplicates(numbers);

       System.out.println("After method call  " + numbers);
       }

    public static void SortAndRemoveDuplicates(LinkedList<Integer>numbers)
        {

        HashSet<Integer>set = new HashSet(numbers); // remove duplicates 

        numbers.clear(); // clear numbers

        numbers.addAll(set); // fill with unique values

        Collections.sort(numbers); // sort values asc

        }
   
}
