/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11;

import java.util.LinkedList;

/**
 *
 * @author Eliise
 */
public class Chapter11Excercise3 {
    
    public static void main(String[] args)  {

        LinkedList<Integer>numbers = new LinkedList();

        numbers.add(0);
        numbers.add(0);
        numbers.add(2);
        numbers.add(0);
        numbers.add(4);
        numbers.add(0);
        numbers.add(6);
        numbers.add(0);
        numbers.add(8);
        numbers.add(0);
        numbers.add(10);
        numbers.add(0);
        numbers.add(12);
        numbers.add(0);
        numbers.add(14);
        numbers.add(0);
        numbers.add(16);

        System.out.println("Array before: \n" + numbers );

        numbers = removeInRange(numbers,0,5,13);

        System.out.println("Array After: \n" + numbers );
    }

    private static LinkedList<Integer> removeInRange(LinkedList<Integer> numbers, int value, int start, int end) {
        
        int times = end - start;
        for(int i = start; i<=times; i++)
        {
            int numberToForRemoving = numbers.get(i);
            
            if(numberToForRemoving == value)
            {
                numbers.remove(i);
            }
        }

        return numbers;
        
    }
}
