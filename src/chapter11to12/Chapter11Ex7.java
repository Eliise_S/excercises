/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11to12;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 *
 * @author Eliise
 */
public class Chapter11Ex7 {
    public static void main(String[] args)  {
     
      ArrayList<Integer>list1 = new ArrayList();
      ArrayList<Integer>list2 = new ArrayList();
   
   
   list1.add(3);
   list1.add(7);
   list1.add(3);
   list1.add(-1);
   list1.add(2);
   list1.add(3);
   list1.add(7);
   list1.add(2);
   list1.add(15);
   list1.add(15);
   
   list2.add(-5);
   list2.add(15);
   list2.add(2);
   list2.add(-1);
   list2.add(7);
   list2.add(15);
   list2.add(35);

      
   System.out.println(list1);
   
   System.out.println(list2);
   
   countCommon(list1,list2);
   
   System.out.println("Those collections have " + countCommon (list1,list2) + " common numbers");
   
   }

   
       public static int countCommon(ArrayList<Integer>list1,ArrayList<Integer>list2)
       {
       
       TreeSet<Integer>matches = new TreeSet(list1);
         
       matches.retainAll(list2);
      
       return matches.size();
              
       }
}
