/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter11to12;

/**
 *
 * @author Eliise
 */
public class Chapter12Ex2 {
     public static void main(String[] args) {
        writeNums(5);
        System.out.println("");
        writeNums(12);
        System.out.println("");
    }
    
    public static void writeNums(int n) {
        if(n < 1)
            throw new IllegalArgumentException("Negative number");
        if(n == 1)
            System.out.print("1");
        else {
            writeNums(n-1);
            System.out.print(","+n);
        }
    }   
}
