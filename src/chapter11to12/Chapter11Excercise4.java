package chapter11to12;


import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eliise
 */
public class Chapter11Excercise4 {
    public static void main(String[] args)  {
     
        
   LinkedList<Integer>numbers = new LinkedList();
   
   numbers.add(15);
   numbers.add(1);
   numbers.add(6);
   numbers.add(12);
   numbers.add(-3);
   numbers.add(4);
   numbers.add(8);
   numbers.add(21);
   numbers.add(2);
   numbers.add(30);
   numbers.add(-1);
   numbers.add(9);
   
   System.out.println("Before method call " + numbers);
   
   partition(numbers,5);
   
   System.out.println("After method call  " + numbers);
   
   
   }

   
    public static void partition(LinkedList<Integer>numbers,int tester)
    {
       int times = numbers.size();
       
       LinkedList<Integer>temporary = new LinkedList<>();
    
       for(int i=0; i<times; i++)
       {  
          int numberToTest = numbers.get(i);
       
             if(numberToTest<tester)
             {
             temporary.add(numberToTest);
             }
       }
       
        for(int i=0; i<times; i++)
       {  
          int numberToTest = numbers.get(i);
       
             if(numberToTest>tester)
             {
             temporary.add(numberToTest);
             }
       }
        
      for(int j=0; j<numbers.size(); j++)
      {
      numbers.set(j,temporary.get(j));
      }
    
    }
}
